"use strict";
exports.__esModule = true;
// Importamos el paquete express
//import express from 'express';
var express = require("express");
var productos = require("./productos");
// Instanciamos una app de express
var app = express();
app.use(express.json());
// Definimos una ruta y su handler correspondiente
app.get('/', function (request, response) {
    response.send('¡Bienvenidos a Express!');
});
//Nuevos endpoints
// Ponemos a escuchar nuestra app de express
app.listen(3000, function () {
    console.info('Servidor escuchando en http://localhost:3000');
});
app.get('/productos', function (request, response) {
    response.send(productos.getStock());
});
app.post('/productos', function (request, response) {
    var body = request.body;
    productos.storeProductos(body);
    response.send('Producto agregado');
});
app["delete"]("/productos/:id", function (request, response) {
    var idProducto = request.params.id;
    productos.deleteProductos(idProducto);
    response.send('Producto eliminado');
});
app.put("/productos/:id", function (request, response) {
    var idProducto = request.params.id;
    var body = request.body;
    productos.updateProductos(idProducto, body);
    response.send('Producto actualizado');
});
